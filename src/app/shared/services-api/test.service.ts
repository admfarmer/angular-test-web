import { Injectable,Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TestService {
  accessToken:any;
  httpOptions: any;

  constructor(
    @Inject('API_URL') private apiUrl: string,
    private httpClient:HttpClient
  ) { 
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }
  }

async select(){
  const _url = `${this.apiUrl}/employee/select`
  return await this.httpClient.get(_url,this.httpOptions).toPromise();
}

async insert(datas:object){
    const _url = `${this.apiUrl}/employee/insert`
  return await this.httpClient.post(_url,datas,this.httpOptions).toPromise();
}

async update(id:any,datas:object){
    const _url = `${this.apiUrl}/employee/update?id=${id}`
  return await this.httpClient.put(_url,datas,this.httpOptions).toPromise();
}

async delete(id:any){
  const _url = `${this.apiUrl}/employee/delete?id=${id}`
  return await this.httpClient.delete(_url,this.httpOptions).toPromise();
}


}
