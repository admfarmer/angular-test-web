import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {AccordionModule} from 'primeng/accordion'; 
import {MenuModule} from 'primeng/menu';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MenubarModule} from 'primeng/menubar';

import {TableModule} from 'primeng/table';
import {PanelModule} from 'primeng/panel';

import { AutoCompleteModule } from "primeng/autocomplete";
import { CalendarModule } from "primeng/calendar";
import { ChipsModule } from "primeng/chips";
import { DropdownModule } from "primeng/dropdown";
import { InputMaskModule } from "primeng/inputmask";
import { InputNumberModule } from "primeng/inputnumber";
import { CascadeSelectModule } from "primeng/cascadeselect";
import { MultiSelectModule } from "primeng/multiselect";
import { InputTextareaModule } from "primeng/inputtextarea";
import { InputTextModule } from "primeng/inputtext";
import {ButtonModule} from 'primeng/button';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AccordionModule,
    MenuModule,
    BrowserAnimationsModule,
    MenubarModule,TableModule,PanelModule,
    FormsModule,
    AutoCompleteModule,CalendarModule,ChipsModule,DropdownModule,InputMaskModule,InputNumberModule,
    CascadeSelectModule,MultiSelectModule,InputTextareaModule,InputTextModule,ButtonModule
  ],
  exports:[
    AccordionModule,
    MenuModule,
    BrowserAnimationsModule,
    MenubarModule,TableModule,PanelModule,
    FormsModule,
    AutoCompleteModule,CalendarModule,ChipsModule,DropdownModule,InputMaskModule,InputNumberModule,
    CascadeSelectModule,MultiSelectModule,InputTextareaModule,InputTextModule,ButtonModule

  ]
})
export class SharedModule { }
