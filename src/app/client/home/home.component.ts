import { Component } from '@angular/core';
import {TestService} from '../../shared/services-api/test.service'
import { AlertService } from '../../shared/alert.service'
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  items:any[]=[];

  id:any;
  firstname:any;
  lastname:any;
  dateStart:any;
  dateEnd:any;
  showFroms:boolean = false;

  constructor(
    private testService:TestService,
    private alertService:AlertService,
  ){

  }
ngOnInit(): void {
  //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
  //Add 'implements OnInit' to the class.
  this.getInfo();
  console.log(this.dateStart);
  
}

async addData(){
  this.showFroms = true;
  this.id = null;
  this.firstname = '';
  this.lastname = '';
  this.dateStart = '';
  this.dateEnd = '';
}

async editData(i:any){
  this.showFroms = true;
  this.id = i.id;
  this.firstname = i.firstname;
  this.lastname = i.lastname;
  this.dateStart = moment(i.dateStart).tz('Asia/Bangkok').format('YYYY-MM-DD');
  this.dateEnd = moment(i.dateEnd).tz('Asia/Bangkok').format('YYYY-MM-DD');
}
async deletData(i:any){
  try {
    let rs:any = await this.testService.delete(i.id);
    console.log(rs);
    this.alertService.success('ลบข้อมูลเรียบร้อยแล้ว');
    this.getInfo();

  } catch (error:any) {
    console.log(error);  
    this.alertService.error('ลบข้อมูลผิดพลาด');
  
  }

}

async cancel(){
  this.showFroms = false;
  this.id = null;
  this.firstname = '';
  this.lastname = '';
  this.dateStart = null;
  this.dateEnd = null;
}

async save(){
  let info:any={};

  let rows:any = {
    firstname : this.firstname,
    lastname : this.lastname,
    dateStart : this.dateStart,
    dateEnd : this.dateEnd
  }
  info.rows = rows;
console.log(info);

  if(this.id){
    try {
      let rs:any = await this.testService.update(this.id,info);
      console.log(rs);
      if(rs){
        this.id = null;
        this.firstname = '';
        this.lastname = '';
        this.dateStart = '';
        this.dateEnd = '';
        this.getInfo();
        this.alertService.success('แก้ไขข้อมูลเรียบร้อยแล้ว');

        this.showFroms = false;
      }
      
    } catch (error:any) {
      console.log(error);
      this.alertService.error('แก้ไขข้อมูลผิดพลาด');

    }

  }else{

    if(this.firstname&&this.lastname&&this.dateStart&&this.dateEnd){
      try {
        let rs:any = await this.testService.insert(info);
        console.log(rs);
        if(rs){
          this.id = null;
          this.firstname = '';
          this.lastname = '';
          this.dateStart = '';
          this.dateEnd = '';
          this.getInfo();
          this.alertService.success('เพิ่มข้อมูลเรียบร้อยแล้ว');
          this.showFroms = false;
        }
      
      } catch (error:any) {
        console.log(error);
        this.alertService.error('เพิ่มข้อมูลผิดพลาด');
      }
    }else{
      console.log('กรอกข้อมูลไม่ครบ');
      this.alertService.error('กรอกข้อมูลไม่ครบ');

    }
  }
}

async getInfo(){
  try {
    let rs:any = await this.testService.select();
    console.log(rs);

    this.items = rs;
    
  } catch (error:any) {
    console.log(error);
    this.alertService.error('อ่านข้อมูลผิดพลาด');
 
  }
}
}
