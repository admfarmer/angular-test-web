import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module';
import {SharedModule} from '../shared/shared.module';
import { ClientComponent } from './client.component';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import { PagesComponent } from './pages/pages.component'

@NgModule({
  declarations: [
    ClientComponent,
    LayoutComponent,
    HomeComponent,
    PagesComponent
  ],
  imports: [
    CommonModule,
    ClientRoutingModule,
    SharedModule
  ]
})
export class ClientModule { }
