import { Component } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent {
  items!: MenuItem[];
  itemsBar!: MenuItem[];

  ngOnInit() {
      this.items = [
          {label: 'Home', icon: 'pi pi-fw pi-plus',routerLink: ['/client/home']},
          {label: 'Pages', icon: 'pi pi-fw pi-download',routerLink:['/client/pages']},
      ];

      this.itemsBar = [
        {label: 'Home', icon: 'pi pi-fw pi-plus',routerLink: ['/client/home']},
        {label: 'Pages', icon: 'pi pi-fw pi-download',routerLink:['/client/pages']},
    ];

  }
}

