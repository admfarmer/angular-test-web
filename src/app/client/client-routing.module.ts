import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import {HomeComponent} from './home/home.component'
import { PagesComponent } from './pages/pages.component'

const routes: Routes = [
  {
    path: 'client', component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path:'home',component: HomeComponent },
      { path:'pages',component: PagesComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
